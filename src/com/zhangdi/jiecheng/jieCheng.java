package com.zhangdi.jiecheng;

import java.util.Scanner;

public class jieCheng {

    public static int fact(int n){
        if(n==0 || n==1){
            return 1;
        }else {
            return n*fact(n-1);
        }
    }
    public static void main(String[] args) {
        int x;
        System.out.println("请您输入一个数");
        Scanner scanner = new Scanner(System.in);
        x = scanner.nextInt();
        System.out.println(x+"的阶乘为"+fact(x));
    }
}
